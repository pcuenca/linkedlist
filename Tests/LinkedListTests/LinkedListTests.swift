import XCTest
import LinkedList

final class LinkedListTests: XCTestCase {
    func testBasics() {
        var list = LinkedList<String>()
        XCTAssertNil(list.value(at: 0), "Empty list should return nil at 0")
        
        let words = ["from", "the", "cold"]
        words.forEach { list.append($0) }
        
        for i in 0..<words.count {
            XCTAssertEqual(list.value(at: i), words[i], "Incorrect value at \(i): \(String(describing: list.value(at: i))), should be \(words[i])")
        }
        
        list.push("in")
        list.push("came")
        
        XCTAssertEqual(list.value(at: 0), "came", "Incorrect value at 0: \(String(describing: list.value(at: 0)))")
    }
    
    func testRemoveLast() {
        var list = LinkedList<String>()
        
        let words = ["from", "the", "cold"]
        words.forEach { list.push($0) }
        
        for i in 0..<words.count {
            let word = words[i]
            let lastWord = list.removeLast()
            XCTAssertEqual(lastWord, word, "removeLast returned \(String(describing: lastWord)), should have been \(word)")
        }
        
        XCTAssertNil(list.removeLast(), "removeLast should return nil on empty list.")
    }
    
    func testRemoveAt() {
        var list = LinkedList<String>()
        
        let words = ["from", "the", "cold"]
        words.forEach { list.append($0) }
                
        // Delete first
        XCTAssertEqual(list.remove(at: 0), "from", "Error deleting at 0")
        
        // Delete last
        XCTAssertEqual(list.remove(at: 1), "cold", "Error deleting at last index")
        
        // Check head and tail state
        XCTAssertEqual(list.head?.value, "the", "Error: incorrect head value")
        XCTAssertEqual(list.tail?.value, "the", "Error: incorrect tail value")
        XCTAssertNil(list.head?.next, "Error: incorrect head.next")
        
        // Delete last
        XCTAssertEqual(list.remove(at: 0), "the", "Error deleting only remaining item")
        XCTAssertNil(list.head, "Error: incorrect head")
        XCTAssertNil(list.tail, "Error: incorrect tail")
        
        // Delete middle ones
        ["came", "in", "from", "the", "cold"].forEach { list.append($0) }
        XCTAssertEqual(list.remove(at: 1), "in", "Error")
        XCTAssertEqual(list.remove(at: 2), "the", "Error")
        XCTAssertEqual(list.remove(at: 1), "from", "Error")
        
        // description
        let desc = String(describing: list)
        XCTAssertEqual(desc, "[came] -> [cold]", "Error describing list, or incorrect state.")
        
        XCTAssert(list.head?.next === list.tail, "Incorrect list state")
        XCTAssertNil(list.tail?.next, "Incorrect list state")
    }
    
    static var allTests = [
        ("testBasics", testBasics),
        ("testRemoveLast", testRemoveLast),
        ("testRemoveAt", testRemoveAt),
    ]
}
