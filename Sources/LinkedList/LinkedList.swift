public class Node<T> {
    public var value: T
    public var next: Node?
    
    init(value: T, next: Node? = nil) {
        self.value = value
        self.next = next
    }
}

extension Node : CustomStringConvertible {
    public var description: String {
        guard let next = next else { return "[\(value)]" }
        return "[\(value)] -> \(next)"
    }
}

public struct LinkedList<T> {
    public var head: Node<T>?
    public var tail: Node<T>?
        
    public init() {}
}

extension LinkedList : CustomStringConvertible {
    public var description: String {
        guard let head = head else { return "Empty list." }
        return "\(head)"
    }
}

extension LinkedList {
    public mutating func append(_ value: T) {
        // Create node
        let newNode = Node(value: value)
        
        // The tail, if it exists, must point to the new node
        if let tail = tail {
            tail.next = newNode
        }
        
        // Update tail and head
        tail = newNode
        if head == nil { head = newNode }
    }

    public mutating func push(_ value: T) {
        head = Node(value: value, next: head)
        if tail == nil { tail = head }
    }

    func node(at index: Int) -> Node<T>? {
        var i = 0
        var node = head
        while node != nil && i < index {
            node = node!.next
            i = i+1
        }
        return node
    }
    
    public func value(at index: Int) -> T? {
        return node(at: index)?.value
    }

    public mutating func insert(_ value: T, at index: Int) {
        guard index > 0 else {
            push(value)
            return
        }
        let prev = node(at: index-1)
        let node = Node(value: value, next: prev?.next)
        prev?.next = node
        if node.next == nil { tail = node }
    }
}

extension LinkedList {
    public mutating func pop() -> T? {
        let node = head
        head = head?.next
        if head == nil { tail = nil }
        return node?.value
    }
    
    public mutating func removeLast() -> T? {
        guard head != nil else { return nil }
                
        guard head !== tail else {
            // Just one element. Note the use of instance identity.
            let result = head?.value
            head = nil
            tail = nil
            return result
        }
        
        let result = tail?.value
        
        var node = head
        while node?.next !== tail { node = node?.next }
        
        node?.next = nil
        tail = node
        
        return result
    }
    
    public mutating func remove(at index: Int) -> T? {
        guard index > 0 else {
            return pop()
        }
        
        guard let prev = node(at: index-1) else { return nil }
        
        let current = prev.next
        prev.next = current?.next
        if current?.next == nil { tail = prev }
        return current?.value
    }
}

extension LinkedList {
    public func forEach(body: (T) -> Void) {
        var current = head
        while current != nil {
            body(current!.value)
            current = current?.next
        }
    }
}

